# Pyscopg + Pydantic Example

This example demonstrates how to use Pydantic models along with
Pyscopg to automatically validate SQL queries with Pydantic.

Here's the official documentation that explains this.

https://www.psycopg.org/psycopg3/docs/advanced/typing.html#example-returning-records-as-pydantic-models

## Instructions to try this out.

1. Edit the docker-compose.yaml file and adjust the username, password and db environment variables to your liking.

2. copy the .env.sample file to .env and adjust the username password db in the URL.

3. `docker-compose up -d`

4. Create and activate a python virtual environment

5. `pip install -r requirements.txt`

6. `python db.py`

